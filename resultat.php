<?php include('connexion.php'); ?>
<!DOCTYPE html>
<html  xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
	<head>
        <link type="text/css" rel="stylesheet" href="style.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
		
<title>Résultats</title>		
<header class="header">
	
<ul id="navigation">
  <li><a href="accueil.php" title="accueil">Accueil</a></li>
</ul>
</header>


<body >
    <h1><br>Résultats</br></h1>   
    <div id="map" style="width:100%; height:90%"></div>
    <script type="text/javascript">
	
	

        var map = L.map('map').setView([48.833, 2.333], 7); 

        var osmLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { 
            attribution: 'Â© OpenStreetMap contributors',
            maxZoom: 19
        });
    
        map.addLayer(osmLayer);

    </script>
    
  
  
   <fieldset>
    <legend>Récapitulatif</legend><br>
   
   <?php
        echo "<b>Niveau</b>: "; 
        echo $_POST['niveau'];

        
        
        echo " ; <b>Departement</b>: ";
        echo $_POST['departement'];
        
        
        echo " ; <b>Formation</b>: ";
        $for = $_POST['formation'];
        echo $for;
        
        
        echo " ; <b>Filière</b>: ";
        echo $_POST['filière'];

	    $sql = "SELECT * FROM TopFormation WHERE formation = '$for' AND filiere = '$filière'  ";
	    $req = $dbh->query($sql);
	    $array = $req->fetch();
		if ($array = true) {
			$requete = "UPDATE TopFormation SET click = click + 1 WHERE formation= '$for' AND filiere = '$filière'";
			$stmt = $dbh->prepare($requete);
			$stmt->execute();
			}
		else {
			$requete = "INSERT INTO TopFormation (formation,click,filiere) VALUES ('$for',1,'$filiere' )";
			$dbh->exec($requete);
			}
		$stmt = $dbh->query("SELECT click FROM TopFormation WHERE formation = '$for' AND filiere = '$filiere'");
		$user = $stmt->fetch();
		echo "<br>";
		echo "<br>";
		echo $user[0]." personne(s) ont été intéressée(s) par cette formation";

	
    ?>
    </fieldset> 
    <fieldset>
    <legend>Résultats</legend><br>


<?php


    $URLstatic = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=1000&sort=-rentree_lib&facet=niveau_lib&refine.rentree_lib=2017-18" ;
    $niveau =  "&refine.niveau_lib=".$_POST['niveau'];
    $dep = "&refine.dep_etab_lib=".$_POST['departement'];
    $formation = "&refine.typ_diplome_lib=".$_POST['formation'];
    $filiere = "&refine.libelle_intitule_1=".$_POST['filière'];
    
    $json = file_get_contents($URLstatic.$niveau.$dep.$filiere.$formation);
    //~ echo $URLstatic.$niveau.$dep.$filiere.$formation;
	$parsed_json = json_decode($json,true);
    $res = sizeof($parsed_json['records']);

    echo "Résultats : ".$res;
    echo "<br>";
    echo "<br>";
    


	for ($cpt = 0 ; $cpt < sizeof($parsed_json['records']); $cpt++){
        
        $matricule = $parsed_json['records'][$cpt]["fields"]["etablissement"];
		echo "<h1>";
		$nomEtab = $parsed_json['records'][$cpt]["fields"]["etablissement_lib"];
		echo "$nomEtab";
		echo "</h1>";
		echo"<p> Académie: ";
		$acaEtab=  $parsed_json['records'][$cpt]["fields"]["aca_etab_lib"];
		echo"$acaEtab";
		echo "<br>" ;
        echo"Région: ";
        $regEtab=  $parsed_json['records'][$cpt]["fields"]["reg_ins_lib"];
		echo"$regEtab";
		echo "<br>" ;
		echo "Diplôme :";
		$typeDip=  $parsed_json['records'][$cpt]["fields"]["typ_diplome_lib"];
		echo $typeDip;
		echo "<br>" ;
		echo "Discipline : ";
		$discipline=  $parsed_json['records'][$cpt]["fields"]["discipline_lib"];
		echo $discipline;
		echo "<br>" ;
		echo "Effectifs : ";
		$effectif=  $parsed_json['records'][$cpt]["fields"]["effectif_total"];
		echo $effectif;
        echo "</p>";

    	$lien2= "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&sort=uo_lib&facet=uai&refine.uai=$matricule";
            $json2  = file_get_contents("$lien2");
                $data2 = json_decode($json2, true);
                $nb2 = sizeof($data2['records']);

                if ( $nb2 >=1) {
                    $site = $data2["records"][0]["fields"]["url"];
                    $adresse = $data2["records"][0]["fields"]["adresse_uai"];
                    $facet_fil2 = $data2["records"][0]["fields"]["coordonnees"]  ;
					echo "<p>Site internet : <a href=intermediaire.php?lien=$site>".$site."</a></p>";
                  
					$sql = "SELECT click FROM compteur_click WHERE URL = '$site'; ";
					
					$req = $dbh->query($sql);
					$array = $req->fetch();
					
                   echo "<p></p>Nombre de clicks: ". $array[0]."</p>";
                   echo "<script>" ;
                   echo "L.marker([$facet_fil2[0], $facet_fil2[1]]).addTo(map).bindPopup(' $nomEtab <br> $adresse ' ).openPopup()";
                   echo " </script> "; }
                else { $site ="";}
              
        
    
	}
?>

    </fieldset>
    </legend>
</body>
</html>
